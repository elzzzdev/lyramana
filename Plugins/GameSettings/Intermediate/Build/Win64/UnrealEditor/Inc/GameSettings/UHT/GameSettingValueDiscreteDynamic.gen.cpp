// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Public/GameSettingValueDiscreteDynamic.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameSettingValueDiscreteDynamic() {}
// Cross Module References
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscrete();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_NoRegister();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_NoRegister();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_NoRegister();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_NoRegister();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_NoRegister();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D();
	GAMESETTINGS_API UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_NoRegister();
	UPackage* Z_Construct_UPackage__Script_GameSettings();
// End Cross Module References
	void UGameSettingValueDiscreteDynamic::StaticRegisterNativesUGameSettingValueDiscreteDynamic()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscrete,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic>()
	{
		return UGameSettingValueDiscreteDynamic::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic);
	UGameSettingValueDiscreteDynamic::~UGameSettingValueDiscreteDynamic() {}
	void UGameSettingValueDiscreteDynamic_Bool::StaticRegisterNativesUGameSettingValueDiscreteDynamic_Bool()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic_Bool);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic_Bool::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscreteDynamic,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic_Bool\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic_Bool" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic_Bool>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic_Bool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Bool.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Bool.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Bool.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic_Bool>()
	{
		return UGameSettingValueDiscreteDynamic_Bool::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic_Bool);
	UGameSettingValueDiscreteDynamic_Bool::~UGameSettingValueDiscreteDynamic_Bool() {}
	void UGameSettingValueDiscreteDynamic_Number::StaticRegisterNativesUGameSettingValueDiscreteDynamic_Number()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic_Number);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic_Number::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscreteDynamic,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic_Number\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic_Number" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic_Number>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic_Number::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Number.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Number.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Number.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic_Number>()
	{
		return UGameSettingValueDiscreteDynamic_Number::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic_Number);
	UGameSettingValueDiscreteDynamic_Number::~UGameSettingValueDiscreteDynamic_Number() {}
	void UGameSettingValueDiscreteDynamic_Enum::StaticRegisterNativesUGameSettingValueDiscreteDynamic_Enum()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic_Enum);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic_Enum::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscreteDynamic,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic_Enum\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic_Enum" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic_Enum>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic_Enum::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Enum.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Enum.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Enum.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic_Enum>()
	{
		return UGameSettingValueDiscreteDynamic_Enum::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic_Enum);
	UGameSettingValueDiscreteDynamic_Enum::~UGameSettingValueDiscreteDynamic_Enum() {}
	void UGameSettingValueDiscreteDynamic_Color::StaticRegisterNativesUGameSettingValueDiscreteDynamic_Color()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic_Color);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic_Color::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscreteDynamic,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic_Color\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic_Color" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic_Color>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic_Color::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Color.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Color.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Color.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic_Color>()
	{
		return UGameSettingValueDiscreteDynamic_Color::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic_Color);
	UGameSettingValueDiscreteDynamic_Color::~UGameSettingValueDiscreteDynamic_Color() {}
	void UGameSettingValueDiscreteDynamic_Vector2D::StaticRegisterNativesUGameSettingValueDiscreteDynamic_Vector2D()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGameSettingValueDiscreteDynamic_Vector2D);
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_NoRegister()
	{
		return UGameSettingValueDiscreteDynamic_Vector2D::StaticClass();
	}
	struct Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameSettingValueDiscreteDynamic,
		(UObject* (*)())Z_Construct_UPackage__Script_GameSettings,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "//////////////////////////////////////////////////////////////////////////\n// UGameSettingValueDiscreteDynamic_Vector2D\n//////////////////////////////////////////////////////////////////////////\n" },
#endif
		{ "IncludePath", "GameSettingValueDiscreteDynamic.h" },
		{ "ModuleRelativePath", "Public/GameSettingValueDiscreteDynamic.h" },
#if !UE_BUILD_SHIPPING
		{ "ToolTip", "UGameSettingValueDiscreteDynamic_Vector2D" },
#endif
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGameSettingValueDiscreteDynamic_Vector2D>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::ClassParams = {
		&UGameSettingValueDiscreteDynamic_Vector2D::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::Class_MetaDataParams), Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D()
	{
		if (!Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Vector2D.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Vector2D.OuterSingleton, Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Vector2D.OuterSingleton;
	}
	template<> GAMESETTINGS_API UClass* StaticClass<UGameSettingValueDiscreteDynamic_Vector2D>()
	{
		return UGameSettingValueDiscreteDynamic_Vector2D::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGameSettingValueDiscreteDynamic_Vector2D);
	UGameSettingValueDiscreteDynamic_Vector2D::~UGameSettingValueDiscreteDynamic_Vector2D() {}
	struct Z_CompiledInDeferFile_FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueDiscreteDynamic_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueDiscreteDynamic_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic, UGameSettingValueDiscreteDynamic::StaticClass, TEXT("UGameSettingValueDiscreteDynamic"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic), 1322926955U) },
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Bool, UGameSettingValueDiscreteDynamic_Bool::StaticClass, TEXT("UGameSettingValueDiscreteDynamic_Bool"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Bool, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic_Bool), 1039914689U) },
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Number, UGameSettingValueDiscreteDynamic_Number::StaticClass, TEXT("UGameSettingValueDiscreteDynamic_Number"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Number, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic_Number), 4124722348U) },
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Enum, UGameSettingValueDiscreteDynamic_Enum::StaticClass, TEXT("UGameSettingValueDiscreteDynamic_Enum"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Enum, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic_Enum), 466545145U) },
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Color, UGameSettingValueDiscreteDynamic_Color::StaticClass, TEXT("UGameSettingValueDiscreteDynamic_Color"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Color, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic_Color), 652902802U) },
		{ Z_Construct_UClass_UGameSettingValueDiscreteDynamic_Vector2D, UGameSettingValueDiscreteDynamic_Vector2D::StaticClass, TEXT("UGameSettingValueDiscreteDynamic_Vector2D"), &Z_Registration_Info_UClass_UGameSettingValueDiscreteDynamic_Vector2D, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGameSettingValueDiscreteDynamic_Vector2D), 413744065U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueDiscreteDynamic_h_3778537370(TEXT("/Script/GameSettings"),
		Z_CompiledInDeferFile_FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueDiscreteDynamic_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueDiscreteDynamic_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
