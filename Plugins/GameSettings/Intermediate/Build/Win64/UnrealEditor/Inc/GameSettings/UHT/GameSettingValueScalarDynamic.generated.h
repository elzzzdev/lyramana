// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "GameSettingValueScalarDynamic.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMESETTINGS_GameSettingValueScalarDynamic_generated_h
#error "GameSettingValueScalarDynamic.generated.h already included, missing '#pragma once' in GameSettingValueScalarDynamic.h"
#endif
#define GAMESETTINGS_GameSettingValueScalarDynamic_generated_h

#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_SPARSE_DATA
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_ACCESSORS
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameSettingValueScalarDynamic(); \
	friend struct Z_Construct_UClass_UGameSettingValueScalarDynamic_Statics; \
public: \
	DECLARE_CLASS(UGameSettingValueScalarDynamic, UGameSettingValueScalar, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameSettings"), NO_API) \
	DECLARE_SERIALIZER(UGameSettingValueScalarDynamic)


#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameSettingValueScalarDynamic(UGameSettingValueScalarDynamic&&); \
	NO_API UGameSettingValueScalarDynamic(const UGameSettingValueScalarDynamic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameSettingValueScalarDynamic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameSettingValueScalarDynamic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGameSettingValueScalarDynamic) \
	NO_API virtual ~UGameSettingValueScalarDynamic();


#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_20_PROLOG
#define FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_SPARSE_DATA \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_ACCESSORS \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_INCLASS_NO_PURE_DECLS \
	FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMESETTINGS_API UClass* StaticClass<class UGameSettingValueScalarDynamic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_LyraStarterGame_Plugins_GameSettings_Source_Public_GameSettingValueScalarDynamic_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
