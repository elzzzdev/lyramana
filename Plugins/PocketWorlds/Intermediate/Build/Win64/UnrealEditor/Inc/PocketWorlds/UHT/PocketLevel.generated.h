// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PocketLevel.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POCKETWORLDS_PocketLevel_generated_h
#error "PocketLevel.generated.h already included, missing '#pragma once' in PocketLevel.h"
#endif
#define POCKETWORLDS_PocketLevel_generated_h

#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_SPARSE_DATA
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_ACCESSORS
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPocketLevel(); \
	friend struct Z_Construct_UClass_UPocketLevel_Statics; \
public: \
	DECLARE_CLASS(UPocketLevel, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevel)


#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevel(UPocketLevel&&); \
	NO_API UPocketLevel(const UPocketLevel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPocketLevel) \
	NO_API virtual ~UPocketLevel();


#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_15_PROLOG
#define FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_SPARSE_DATA \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_ACCESSORS \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_INCLASS_NO_PURE_DECLS \
	FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POCKETWORLDS_API UClass* StaticClass<class UPocketLevel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_LyraStarterGame_Plugins_PocketWorlds_Source_Public_PocketLevel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
