// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySystemComponent.h"
#include "LyraAttributeSet.h"
#include "NativeGameplayTags.h"
#include "LyraManaSet.generated.h"

UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_Lyra_DecrementMana_Message);

UCLASS(BlueprintType)
class LYRAGAME_API ULyraManaSet : public ULyraAttributeSet
{
	GENERATED_BODY()

public:
	ULyraManaSet();

	ATTRIBUTE_ACCESSORS(ULyraManaSet, Mana);
	ATTRIBUTE_ACCESSORS(ULyraManaSet, MaxMana);
	ATTRIBUTE_ACCESSORS(ULyraManaSet, IncrementMana);
	ATTRIBUTE_ACCESSORS(ULyraManaSet, DecrementMana);

	mutable FLyraAttributeEvent OnManaChanged;

	mutable FLyraAttributeEvent OnMaxManaChanged;

	mutable FLyraAttributeEvent OnOutOfMana;

protected:
	UFUNCTION()
	void OnRep_Mana(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	void OnRep_MaxMana(const FGameplayAttributeData& OldValue);

	virtual bool PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	virtual void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;

	void ClampAttribute(const FGameplayAttribute& Attribute, float& NewValue) const;

private:
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_Mana, Category = "Lyra|Mana", Meta = (HideFromModifiers, AllowPrivateAccess = true))
	FGameplayAttributeData Mana;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_MaxMana, Category = "Lyra|Mana", Meta = (AllowPrivateAccess = true))
	FGameplayAttributeData MaxMana;

	bool bOutOfMana;

	float MaxManaBeforeAttributeChange;
	float ManaBeforeAttributeChange;

	// -------------------------------------------------------------------
	//	Meta Attribute (please keep attributes that aren't 'stateful' below
	// -------------------------------------------------------------------

	UPROPERTY(BlueprintReadOnly, Category="Lyra|Health", Meta=(AllowPrivateAccess=true))
	FGameplayAttributeData IncrementMana;

	UPROPERTY(BlueprintReadOnly, Category="Lyra|Health", Meta=(HideFromModifiers, AllowPrivateAccess=true))
	FGameplayAttributeData DecrementMana;
};
