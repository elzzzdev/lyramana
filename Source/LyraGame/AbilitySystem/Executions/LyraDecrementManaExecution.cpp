// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Executions/LyraDecrementManaExecution.h"

#include "AbilitySystem/Attributes/LyraCombatSet.h"
#include "AbilitySystem/Attributes/LyraManaSet.h"

#include UE_INLINE_GENERATED_CPP_BY_NAME(LyraDecrementManaExecution)

struct FDecrementManaStatics
{
	FGameplayEffectAttributeCaptureDefinition BaseDecrementManaDef;

	FDecrementManaStatics()
	{
		BaseDecrementManaDef = FGameplayEffectAttributeCaptureDefinition(ULyraCombatSet::GetBaseDecrementManaAttribute(), EGameplayEffectAttributeCaptureSource::Source, true);
	}
};

static FDecrementManaStatics& DecrementManaStatics()
{
	static FDecrementManaStatics Statics;
	return Statics;
}

ULyraDecrementManaExecution::ULyraDecrementManaExecution()
{
	RelevantAttributesToCapture.Add(DecrementManaStatics().BaseDecrementManaDef);
}

void ULyraDecrementManaExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
#if WITH_SERVER_CODE
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluateParameters;
	EvaluateParameters.SourceTags = SourceTags;
	EvaluateParameters.TargetTags = TargetTags;

	float BaseDecrementMana = 0.0f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DecrementManaStatics().BaseDecrementManaDef, EvaluateParameters, BaseDecrementMana);

	const float DecrementManaDone = FMath::Max(0.0f, BaseDecrementMana);

	if (DecrementManaDone > 0.0f)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(ULyraManaSet::GetDecrementManaAttribute(), EGameplayModOp::Additive, DecrementManaDone));
	}
#endif // #if WITH_SERVER_CODE
}
