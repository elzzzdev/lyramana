// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "LyraDecrementManaExecution.generated.h"

UCLASS()
class LYRAGAME_API ULyraDecrementManaExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:
	ULyraDecrementManaExecution();

protected:
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
};
