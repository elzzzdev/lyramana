// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Executions/LyraIncrementManaExecution.h"

#include "AbilitySystem/Attributes/LyraCombatSet.h"
#include "AbilitySystem/Attributes/LyraManaSet.h"

#include UE_INLINE_GENERATED_CPP_BY_NAME(LyraIncrementManaExecution)

struct FIncrementManaStatics
{
	FGameplayEffectAttributeCaptureDefinition BaseIncrementManaDef;

	FIncrementManaStatics()
	{
		BaseIncrementManaDef = FGameplayEffectAttributeCaptureDefinition(ULyraCombatSet::GetBaseIncrementManaAttribute(), EGameplayEffectAttributeCaptureSource::Source, true);
	}
};

static FIncrementManaStatics& IncrementManaStatics()
{
	static FIncrementManaStatics Statics;
	return Statics;
}

ULyraIncrementManaExecution::ULyraIncrementManaExecution()
{
	RelevantAttributesToCapture.Add(IncrementManaStatics().BaseIncrementManaDef);
}

void ULyraIncrementManaExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
#if WITH_SERVER_CODE
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluateParameters;
	EvaluateParameters.SourceTags = SourceTags;
	EvaluateParameters.TargetTags = TargetTags;

	float BaseIncrementMana = 0.0f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(IncrementManaStatics().BaseIncrementManaDef, EvaluateParameters, BaseIncrementMana);

	const float IncrementManaDone = FMath::Max(0.0f, BaseIncrementMana);

	if (IncrementManaDone > 0.0f)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(ULyraManaSet::GetIncrementManaAttribute(), EGameplayModOp::Additive, IncrementManaDone));
	}
#endif // #if WITH_SERVER_CODE
}