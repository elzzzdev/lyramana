// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "LyraIncrementManaExecution.generated.h"

UCLASS()
class LYRAGAME_API ULyraIncrementManaExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:
	ULyraIncrementManaExecution();

protected:
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
};
