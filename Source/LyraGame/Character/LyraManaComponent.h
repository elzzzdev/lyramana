// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/GameFrameworkComponent.h"
#include "LyraManaComponent.generated.h"

struct FGameplayEffectSpec;
class ULyraManaComponent;
class ULyraAbilitySystemComponent;
class ULyraManaSet;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FLyraMana_AttributeChanged, ULyraManaComponent*, ManaComponent, float, OldValue, float, NewValue, AActor*, Instigator);

UCLASS(BlueprintType)
class LYRAGAME_API ULyraManaComponent : public UGameFrameworkComponent
{
	GENERATED_BODY()

public:
	ULyraManaComponent(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintPure, Category = "Lyra|Mana")
	static ULyraManaComponent* FindManaComponent(const AActor* Actor) { return (Actor ? Actor->FindComponentByClass<ULyraManaComponent>() : nullptr); }

	UFUNCTION(BlueprintCallable, Category = "Lyra|Mana")
	void InitializeWithAbilitySystem(ULyraAbilitySystemComponent* InASC);

	UFUNCTION(BlueprintCallable, Category = "Lyra|Mana")
	void UninitializeFromAbilitySystem();

	UFUNCTION(BlueprintCallable, Category = "Lyra|Mana")
	float GetMana() const;

	UFUNCTION(BlueprintCallable, Category = "Lyra|Mana")
	float GetMaxMana() const;

	UFUNCTION(BlueprintCallable, Category = "Lyra|Mana")
	float GetManaNormalized() const;

	UPROPERTY(BlueprintAssignable)
	FLyraMana_AttributeChanged OnManaChanged;

	UPROPERTY(BlueprintAssignable)
	FLyraMana_AttributeChanged OnMaxManaChanged;

protected:
	virtual void OnUnregister() override;

	virtual void HandleManaChanged(AActor* Instigator, AActor* Causer, const FGameplayEffectSpec* EffectSpec, float Magnitude, float OldValue, float NewValue);
	virtual void HandleMaxManaChanged(AActor* Instigator, AActor* Causer, const FGameplayEffectSpec* EffectSpec, float Magnitude, float OldValue, float NewValue);
	virtual void HandleOutOfMana(AActor* Instigator, AActor* Causer, const FGameplayEffectSpec* EffectSpec, float Magnitude, float OldValue, float NewValue);

	UPROPERTY()
	TObjectPtr<ULyraAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY()
	TObjectPtr<const ULyraManaSet> ManaSet;
};
